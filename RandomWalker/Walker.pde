class Walker {
 float xPosition;
 float yPosition;
 
 void render() {
  circle(xPosition, yPosition, 30); 
 }
 
 void randomWalk() {
      int decision = int(random(8)); 
      int distance = 10;
      color col = color(random(255), random(255), random(255), random(255));
      alpha(col);
      fill(col);
      noStroke();
      
      if (decision == 0) 
      yPosition+=distance;
      else if (decision == 1)
      yPosition-=distance;
      else if (decision == 2)
      xPosition-=distance;
      else if (decision == 3)
      xPosition+=distance;
      else if (decision == 4){
      xPosition+= distance; 
      yPosition+= distance;
      }
      else if (decision == 5) {
        xPosition += distance;
        yPosition -= distance;
      }
      else if (decision == 6) {
        xPosition -= distance;
        yPosition += distance;
      }
      else if (decision == 7) {
       xPosition -= distance;
       yPosition -= distance;
      }
 }
}
