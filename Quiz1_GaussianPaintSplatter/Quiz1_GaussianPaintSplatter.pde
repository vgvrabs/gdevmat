void setup() {
 size(1920, 1080, P3D);
 background(0);
 
}

void draw() {
  camera(0,0, -(height/2.0f) / tan(PI * 30/180.0f),0,0,0,0,-1,0);
  
  // for x pos
  float xGauss = randomGaussian();
  float xMean = 10;
  float xDeviation = 1000;
  float xPos = (xDeviation * xGauss) + xMean;
  
  // for y pos
  float yPos = random(-height, height);

 // for size
 float sizeGauss = randomGaussian();
 float sizeMean = 150;
 float sizeDeviation = 100;
 float size = (sizeDeviation * sizeGauss) + sizeMean;
 
  noStroke();
  color col = color(random(255), random(255), random(255), random(10, 100));
  alpha(col);
  fill(col);
  
  circle(xPos, yPos, size);
  
  if(frameCount == 1000) {
   clear();
   frameCount = 0;
    
  }
  
  
  
}

  
